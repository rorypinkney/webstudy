TASK_TYPES = (
    ('', "Type"),
    (1, "Reading"),
    (2, "Writing"),
    (3, "Programming"),
    (4, "Revising"),
    (5, "Other"),
)
