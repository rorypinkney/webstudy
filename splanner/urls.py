from django.urls import path,include

from . import views


# url patterns when the server receives a URL request it iterates through this list to find the first matching one
urlpatterns = [
    path('', views.home, name='home'),
    path('add-module/', views.home, name='add module'),
    path('<int:module_id>/', views.module, name='module'),
    path('<int:module_id>/edit-module/', views.module, name='edit module'),
    path('<int:module_id>/del-module/', views.module, name='delete module'),
    path('<int:module_id>/add-assessment/', views.module, name='add assessment'),
    path('<int:module_id>/<int:assessment_id>/', views.assessment, name='assessment'),
    path('<int:module_id>/<int:assessment_id>/edit-assessment/', views.assessment, name='edit assessment'),
    path('<int:module_id>/<int:assessment_id>/del-assessment/', views.assessment, name='delete assessment'),
    path('<int:module_id>/<int:assessment_id>/add-task/', views.assessment, name='add task'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/', views.task, name='task'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/edit-task/', views.task, name='edit task'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/del-task/', views.task, name='delete task'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/add-activity/', views.task, name='add activity'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/edit-activity/', views.task, name='edit activity'),
    path('<int:module_id>/<int:assessment_id>/<int:task_id>/del-activity/', views.task, name='delete activity'),
    path('', include('django.contrib.auth.urls')),
]
