from django.shortcuts import render, redirect, HttpResponseRedirect
from django.urls import resolve
from .forms import AddModuleForm, EditModuleForm, \
                   AddAssessmentForm, EditAssessmentForm, \
                   AddTaskForm, EditTaskForm, AddActivityForm, \
                   EditActivityForm
from datetime import date
from django.contrib import messages
import operator

from .models import *


# fetches the relevant data in the request from the models, formats it and passes it to the home.html template
def home(request):                                      # # HOME VIEW # #
    current_user = request.user

    if request.method == 'POST':
        form = AddModuleForm(request.POST, user=current_user, request=request.method)
        if form.is_valid():
            name = form.cleaned_data['name']
            weight = form.cleaned_data['weight']
            module = Module(name=name, weight=weight, user=current_user)
            module.save()
            messages.add_message(request, messages.SUCCESS, 'Successfully added "' + name + '"')
        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)

    modules = None
    due_soon = []
    priority_assessment = None
    time_remaining = None
    due_soon_progress = 0
    year_tracked = 0
    if current_user.is_authenticated:
        modules = Module.objects.filter(user=current_user)

        startdate = date.today()
        enddate = startdate + timezone.timedelta(days=14)
        assessments = []
        for module in modules:
            year_tracked += module.weight
            due_soon.extend(Assessment.objects.filter(module=module).filter(due_date__range=[startdate, enddate]))
            assessments.extend(Assessment.objects.filter(module=module))
        year_tracked = round(year_tracked)

        if due_soon:
            due_soon = sorted(due_soon, key=operator.attrgetter('priority'))
            priority_assessment = due_soon[0]
            for assessment in due_soon:
                due_soon_progress += assessment.progress
            due_soon_progress = round(due_soon_progress / len(due_soon))
            time_remaining = (priority_assessment.due_date - timezone.now().date()).days
        elif assessments:
            assessments = sorted(assessments, key=operator.attrgetter('priority'))
            priority_assessment = assessments[0]

    form = AddModuleForm(auto_id='add_%s', user=current_user, request=request.method)
    return render(request, 'splanner/home.html',
                  {'modules': modules, 'addModuleForm': form, 'yearTracked': year_tracked,
                   'dueSoon': due_soon, 'dueSoonProgress': due_soon_progress,
                   'priorityAssessment': priority_assessment, 'timeRemaining': time_remaining})


# fetches the relevant data in the request from the models, formats it and passes it to the module.html template
def module(request, module_id):                         # # MODULE VIEW # #
    current_user = request.user

    if request.method == 'POST':
        url_name = resolve(request.path).url_name           # checking URL for form type

# add assessment form processing
        if url_name == 'add assessment':
            form = AddAssessmentForm(request.POST, module_id=module_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                name = form.cleaned_data['name']
                due_date = form.cleaned_data['due_date']
                start_date = form.cleaned_data['start_date']
                weight = form.cleaned_data['weight']
                period = (due_date - start_date).days

                total_weight = Assessment.objects.filter(module_id=module_id).aggregate(Sum('weight'))['weight__sum']
                total_weight = int(total_weight or 0) + weight
                if total_weight <= 100:
                    assessment = Assessment(name=name, module_id=module_id, due_date=due_date,
                                            start_date=start_date, weight=weight, period=period)
                    assessment.save()
                    messages.add_message(request, messages.SUCCESS, 'Successfully added "' + name + '"')

# edit module form processing
        elif url_name == 'edit module':
            form = EditModuleForm(request.POST, user=current_user, module_id=module_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                name = form.cleaned_data['name']
                weight = form.cleaned_data['weight']

                module = Module.objects.get(pk=module_id)
                module.name = name
                module.weight = weight
                module.save()
                messages.add_message(request, messages.SUCCESS, 'Successfully edited "' + name + '"')

# delete module processing
        elif url_name == 'delete module':
            messages.add_message(request, messages.SUCCESS, 'Successfully deleted "' +
                                 Module.objects.get(pk=module_id).name + '"')
            Module.objects.filter(pk=module_id).delete()
            next = request.POST.get('next', '/')
            return HttpResponseRedirect(next)

        next = request.POST.get('next', '/' + str(module_id) + '/')  # remove form section of url /add-etc
        return HttpResponseRedirect(next)

    priority_assessment = None
    module_tracked = 0
    if current_user.is_authenticated:
        this_module = Module.objects.get(pk=module_id)
        assessments = Assessment.objects.filter(module_id=module_id).order_by('due_date')

        if assessments:
            priority_assessment = assessments[0]
            for assessment in assessments:
                module_tracked += assessment.weight
                if assessment.priority > priority_assessment.priority:
                    priority_assessment = assessment
            module_tracked = round(module_tracked)
    else:
        return redirect(home)
    add_form = AddAssessmentForm(auto_id='add_%s', module_id=module_id, request=request.method)
    edit_form = EditModuleForm(auto_id='edit_%s', module_id=module_id, user=current_user, request=request.method)
    return render(request, 'splanner/module.html',
                  {'assessments': assessments, 'module': this_module,
                   'addAssessmentForm': add_form, 'editModuleForm': edit_form,
                   'moduleTracked': module_tracked, 'priorityAssessment': priority_assessment})


# fetches the relevant data in the request from the models, formats it and passes it to the assessment.html template
def assessment(request, module_id, assessment_id):      # # ASSESSMENT VIEW # #
    current_user = request.user

    if request.method == 'POST':
        url_name = resolve(request.path).url_name               # checking URL for form type

# add task form processing
        if url_name == 'add task':
            form = AddTaskForm(request.POST, assessment_id=assessment_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                name = form.cleaned_data['name']
                part = form.cleaned_data['part']
                type = form.cleaned_data['type']
                weight = form.cleaned_data['weight']

                parts = Task.objects.filter(assessment_id=assessment_id).values_list('part', flat=True)
                total_weight = Task.objects.filter(assessment_id=assessment_id).aggregate(Sum('weight'))['weight__sum']
                total_weight = int(total_weight or 0) + weight
                if total_weight <= 100 and part not in parts:
                    task = Task(name=name, assessment_id=assessment_id, part=part, type=type, weight=weight)
                    task.save()
                    messages.add_message(request, messages.SUCCESS, 'Successfully added "' + name + '"')

# edit assessment form processing
        elif url_name == 'edit assessment':
            form = EditAssessmentForm(request.POST, module_id=module_id,
                                      assessment_id=assessment_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                name = form.cleaned_data['name']
                due_date = form.cleaned_data['due_date']
                start_date = form.cleaned_data['start_date']
                weight = form.cleaned_data['weight']
                period = (due_date - start_date).days
                print(period)

                assessment = Assessment.objects.get(pk=assessment_id)
                assessment.name = name
                assessment.due_date = due_date
                assessment.start_date = start_date
                assessment.weight = weight
                assessment.period = period
                assessment.save()
                messages.add_message(request, messages.SUCCESS, 'Successfully edited "' + name + '"')

# delete assessment processing
        elif url_name == 'delete assessment':
            messages.add_message(request, messages.SUCCESS, 'Successfully deleted "' +
                                 Assessment.objects.get(pk=assessment_id).name + '"')
            Assessment.objects.filter(pk=assessment_id).delete()
            next = request.POST.get('next', '/' + str(module_id) + '/')
            return HttpResponseRedirect(next)

        next = request.POST.get('next', '/' + str(module_id) + '/' + str(assessment_id) + '/')
        return HttpResponseRedirect(next)

    assessment_tracked = 0
    if current_user.is_authenticated:
        this_module = Module.objects.get(pk=module_id)
        this_assessment = Assessment.objects.get(pk=assessment_id)
        tasks = Task.objects.filter(assessment_id=assessment_id).order_by('part')

        assessment_weight = (this_assessment.weight * this_module.weight)/100
        recommended_period = round(assessment_weight * 2)
        for task in tasks:
            assessment_tracked += task.weight
        assessment_tracked = round(assessment_tracked)
    else:
        return redirect(home)
    add_form = AddTaskForm(auto_id='add_%s', assessment_id=assessment_id, request=request.method)
    edit_form = EditAssessmentForm(auto_id='edit_%s', module_id=module_id, assessment_id=assessment_id, request=request.method)
    return render(request, 'splanner/assessment.html',
                  {'module': this_module, 'assessment': this_assessment, 'tasks': tasks,
                   'addTaskForm': add_form, 'editAssessmentForm': edit_form,
                   'assessmentTracked': assessment_tracked, 'assessmentWeight': assessment_weight,
                   'recommendedPeriod': recommended_period})


# fetches the relevant data in the request from the models, formats it and passes it to the task.html template
def task(request, module_id, assessment_id, task_id):
    current_user = request.user

    if request.method == 'POST':
        url_name = resolve(request.path).url_name               # checking URL for form type

# add activity form processing
        if url_name == 'add activity':                          # add-activity form
            form = AddActivityForm(request.POST, task_id=task_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                date = form.cleaned_data['date']
                duration = form.cleaned_data['duration']
                progress = form.cleaned_data['progress']
                comments = form.cleaned_data['comments']

                current_progress = Activity.objects.filter(task_id=task_id).aggregate(Sum('progress'))['progress__sum']
                current_progress = int(current_progress or 0)
                if progress <= 100 - current_progress:
                    activity = Activity(task_id=task_id, date=date, duration=duration, progress=progress,
                                        comments=comments)
                    activity.save()
                    messages.add_message(request, messages.SUCCESS, 'Successfully added "' + str(date)
                                         + ' - ' + str(progress) + '%"')

# edit activity form processing
        elif url_name == 'edit activity':
            form = EditActivityForm(request.POST)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                date = form.cleaned_data['date']
                duration = form.cleaned_data['duration']
                progress = form.cleaned_data['progress']
                comments = form.cleaned_data['comments']
                activity_id = request.POST['activity_id']

                activity = Activity.objects.get(pk=activity_id)
                activity.date = date
                activity.duration = duration
                activity.progress = progress
                activity.comments = comments
                activity.save()
                messages.add_message(request, messages.SUCCESS, 'Successfully edited "' + str(date)
                                     + ' - ' + str(progress) + '%"')

# delete activity processing
        elif url_name == 'delete activity':
            activity_id = request.POST['activity_id']
            activity = Activity.objects.get(pk=activity_id)
            messages.add_message(request, messages.SUCCESS, 'Successfully deleted "' + str(activity.date)
                                 + ' - ' + str(activity.progress) + '%"')

            Activity.objects.filter(pk=activity_id).delete()
            next = request.POST.get('next', '/' + str(module_id) + '/' + str(assessment_id) + '/' + str(task_id) + '/')
            return HttpResponseRedirect(next)

# edit task form processing
        elif url_name == 'edit task':
            form = EditTaskForm(request.POST, assessment_id=assessment_id, task_id=task_id, request=request.method)
            if not form.has_changed():
                form.is_bound = False
            if form.is_valid():
                name = form.cleaned_data['name']
                part = form.cleaned_data['part']
                type = form.cleaned_data['type']
                weight = form.cleaned_data['weight']

                task = Task.objects.get(pk=task_id)
                task.name = name
                task.part = part
                task.type = type
                task.weight = weight
                task.save()
                messages.add_message(request, messages.SUCCESS, 'Successfully edited "' + name + '"')

# delete task processing
        elif url_name == 'delete task':
            messages.add_message(request, messages.SUCCESS, 'Successfully deleted "' +
                                 Task.objects.get(pk=task_id).name + '"')
            Task.objects.filter(pk=task_id).delete()
            next = request.POST.get('next', '/' + str(module_id) + '/' + str(assessment_id) + '/')
            return HttpResponseRedirect(next)

        next = request.POST.get('next', '/' + str(module_id) + '/' + str(assessment_id) + '/' + str(task_id) + '/')
        return HttpResponseRedirect(next)

    if current_user.is_authenticated:
        this_module = Module.objects.get(pk=module_id)
        this_assessment = Assessment.objects.get(pk=assessment_id)
        this_task = Task.objects.get(pk=task_id)
        tasks = Task.objects.filter(assessment_id=assessment_id)
        activities = Activity.objects.filter(task_id=task_id).order_by('date')

        cumulative_progress = []
        curr = 0
        for activity in activities:
            curr += activity.progress
            cumulative_progress.append(curr)

        task_weight_module = (this_task.weight * this_assessment.weight) / 100
        task_weight_year = (task_weight_module * this_module.weight) / 100
    else:
        return redirect(home)
    add_form = AddActivityForm(auto_id='add_%s', task_id=task_id, request=request.method)
    edit_task_form = EditTaskForm(auto_id='edit_task_%s', assessment_id=assessment_id, task_id=task_id,
                                  request=request.method)
    edit_activity_form = EditActivityForm(auto_id='edit_activity_%s')
    return render(request, 'splanner/task.html',
                  {'module': this_module, 'assessment': this_assessment, 'task': this_task,
                   'tasks': tasks, 'activities': activities, 'progress_list': cumulative_progress,
                   'addActivityForm': add_form, 'editTaskForm': edit_task_form,
                   'editActivityForm': edit_activity_form, 'taskWeightModule': task_weight_module,
                   'taskWeightYear': task_weight_year})
