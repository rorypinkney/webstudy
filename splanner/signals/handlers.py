from splanner import models
from django.db.models.signals import post_delete
from django.dispatch import receiver


# This module handles the django database post-delete signals to properly update all the tables progresses and
# priorities if any table data is deleted by a user. All deletions cascade in order to prevent unreachable data
# remaining in child tables, so any parent table's 'progress' needs to be recalculated to reflect the dataset underneath
# them. There is no requirement for a post-delete signal on the module table because all progresses have been deleted.


# handles a deletion of an assessment by recalculating module progress and calling module.save()
@receiver(post_delete, sender=models.Assessment)
def assessment_delete(instance, **kwargs):
    module = models.Module.objects.get(pk=instance.module_id)

    progress = 0
    for task in models.Task.objects.filter(assessment=instance):
        progress += task.progress

    module.progress -= progress * instance.weight / 100
    module.save()


# handles a deletion of an task by recalculating assessment progress and calling assessment.save(), which then
# propagates up the model's overridden .save() functions in the normal fashion, updating all the parent's progresses
@receiver(post_delete, sender=models.Task)
def task_delete(instance, **kwargs):
    assessment = models.Assessment.objects.get(pk=instance.assessment_id)

    progress = 0
    for activity in models.Activity.objects.filter(task=instance):
        progress += activity.progress

    assessment.progress -= progress * instance.weight/100
    assessment.save()


# handles a deletion of an activity by recalculating task progress and calling task.save(), which then
# propagates up the model's overridden .save() functions in the normal fashion, updating all the parent's progresses
@receiver(post_delete, sender=models.Activity)
def activity_delete(instance, **kwargs):
    task = models.Task.objects.get(pk=instance.task_id)
    task.progress -= instance.progress
    task.save()
