from django.apps import AppConfig


class SplannerConfig(AppConfig):
    name = 'splanner'

    def ready(self):
        import splanner.signals.handlers
