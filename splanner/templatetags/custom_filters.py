from django import template
from splanner import task_types

register = template.Library()


@register.filter
def list_item(list, i):
    return list[i]

@register.filter
def task_name(i):
    return task_types.TASK_TYPES[i][1]
