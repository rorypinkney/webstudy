from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.utils import timezone

from splanner.task_types import TASK_TYPES


class Module(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    weight = models.FloatField(validators=[MaxValueValidator(100)])
    progress = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(100)])

    def __str__(self):
        return self.name


class Assessment(models.Model):
    name = models.CharField(max_length=255)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    start_date = models.DateField('start date')
    due_date = models.DateField('due date')
    period = models.PositiveIntegerField()
    weight = models.FloatField(validators=[MaxValueValidator(100)])
    progress = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(100)])
    priority = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    # overridden save function sums the weighted progresses of all the assessments in this module updating the
    # 'progress' field in the parent module appropriately. Calls module.save() propagating the progress upwards into
    # the final table 'Module
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        progress = 0
        assessments = Assessment.objects.filter(module_id=self.module_id)
        for assessment in assessments:
            progress += (assessment.progress * assessment.weight)/100

        module = Module.objects.get(pk=self.module_id)
        module.progress = progress
        module.save()


class Task(models.Model):
    name = models.CharField(max_length=255)
    assessment = models.ForeignKey(Assessment, on_delete=models.CASCADE)
    part = models.PositiveIntegerField()
    type = models.IntegerField(choices=TASK_TYPES)
    weight = models.FloatField(validators=[MaxValueValidator(100)])
    progress = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(100)])

    def __str__(self):
        return self.name

    # overridden save function sums the weighted progresses of all the tasks in this assessment updating the 'progress'
    # field in the parent assessment appropriately. It also calculates the new assessment priority based on this
    # updated progress. Calls assessment.save() propagating the progress upwards through all the above tables.
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        progress = 0
        tasks = Task.objects.filter(assessment_id=self.assessment_id)
        for task in tasks:
            progress += (task.progress * task.weight)/100

        assessment = Assessment.objects.get(pk=self.assessment_id)
        assessment.progress = progress

        remaining_time = (assessment.due_date - timezone.now().date()).days
        print(remaining_time)
        assessment.priority = ((100-assessment.progress)*assessment.weight*assessment.module.weight)/(remaining_time+1)
        assessment.save()


class Activity(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    comments = models.TextField(blank=True, null=True)
    date = models.DateField()
    duration = models.PositiveIntegerField()
    progress = models.PositiveIntegerField(default=0, validators=[MaxValueValidator(100)])

    def __str__(self):
        return str(self.date) + ' - ' + str(self.progress)

    # overridden save function adds together the progresses of all the other activities in this task and updates the
    # the 'progress' field in the parent task appropriately. Calls task.save() propagating the progress upwards through
    # all the above tables.
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        progress = Activity.objects.filter(task_id=self.task_id).aggregate(Sum('progress'))['progress__sum']
        progress = int(progress or 0)
        task = Task.objects.get(pk=self.task_id)
        task.progress = progress
        task.save()
