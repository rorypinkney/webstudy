from django import forms
from django.db.models import Sum
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from splanner import models
from splanner.task_types import TASK_TYPES


# forms modelled as CLASSES not as FUNCTIONS, look up the documentation for class based forms

class SignUpForm(UserCreationForm):     # extend the UserCreationForm
    # provide additional custom fields
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.',
                                 widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.',
                                widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.',
                             widget=forms.TextInput(attrs={'placeholder': 'Email'}))

    class Meta:
        model = User
        # defines order of the columns
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Username'
        self.fields['password1'].widget.attrs['placeholder'] = 'Password'
        self.fields['password2'].widget.attrs['placeholder'] = 'Password Confirmation'


class AddModuleForm(forms.ModelForm):
    class Meta:
        model = models.Module               # associated model to get fields from
        fields = ('name', 'weight')                  # list of fields from the model to be in the form
        # widgets allows you to modify the html markup of the form before it's put into the django variable in the view
        #   'form-control' is a bootstrap class which makes inputs look nice
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Module Name'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.user = kwargs.pop('user')
        super(AddModuleForm, self).__init__(*args, **kwargs)

        if self.request == 'GET' and self.user.is_authenticated:
            weight = models.Module.objects.filter(user=self.user).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0)
            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))


class EditModuleForm(forms.ModelForm):
    class Meta:
        model = models.Module               # associated model to get fields from
        fields = ('name', 'weight')                  # list of fields from the model to be in the form
        # widgets allows you to modify the html markup of the form before it's put into the django variable in the view
        #   'form-control' is a bootstrap class which makes inputs look nice
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Module Name'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.user = kwargs.pop('user')
        self.module_id = kwargs.pop('module_id')
        super(EditModuleForm, self).__init__(*args, **kwargs)

        if self.request == 'GET' and self.user.is_authenticated:
            weight = models.Module.objects.filter(user=self.user).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0) - models.Module.objects.get(pk=self.module_id).weight
            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))


class AddAssessmentForm(forms.ModelForm):
    class Meta:
        model = models.Assessment           # associated model to get fields from
        exclude = ('module', 'progress', 'period', 'priority')  # list of fields from the model to EXCLUDE from the form
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Assessment Name'}),
            'due_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Due Date'}),
            'start_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Start Date'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.module_id = kwargs.pop('module_id')
        super(AddAssessmentForm, self).__init__(*args, **kwargs)

        # this dynamically sets the validation of the 'weight' field in the AssessmentForm, this __init__ is called
        # every time the AssessmentForm object is created in the Module view
        if self.request == 'GET':
            # dynamic weight limit is calculated by SELECTing all assessments and aggregating their weights and taking
            # that number away from 100 to give the percentage unallocated.
            weight = models.Assessment.objects.filter(module_id=self.module_id).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0)

            # that dynamically created weight is placed into the html markup here
            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))


class EditAssessmentForm(forms.ModelForm):
    class Meta:
        model = models.Assessment
        exclude = ('module', 'progress', 'period', 'priority')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Assessment Name'}),
            'due_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Due Date'}),
            'start_date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Start Date'}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.module_id = kwargs.pop('module_id')
        self.assessment_id = kwargs.pop('assessment_id')
        super(EditAssessmentForm, self).__init__(*args, **kwargs)

        if self.request == 'GET':
            weight = models.Assessment.objects.filter(module_id=self.module_id).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0) - models.Assessment.objects.get(pk=self.assessment_id).weight

            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))


class AddTaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        exclude = ('assessment', 'progress')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Task Name'}),
            'type': forms.Select(attrs={'class': 'form-control'}, choices=TASK_TYPES),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.assessment_id = kwargs.pop('assessment_id')
        super(AddTaskForm, self).__init__(*args, **kwargs)

        if self.request == 'GET':
            weight = models.Task.objects.filter(assessment_id=self.assessment_id
                                                ).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0)
            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))
            self.fields['part'] = forms.IntegerField(min_value=1, widget= forms.NumberInput(
                                                         attrs={
                                                             'class': 'form-control',
                                                             'placeholder': 'TaskNumber',
                                                         }
                                                     ))


class EditTaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        exclude = ('assessment', 'progress')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Task Name'}),
            'part': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Task Number'}),
            'type': forms.Select(attrs={'class': 'form-control'}, choices=TASK_TYPES),
            'weight': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Weight'})
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.assessment_id = kwargs.pop('assessment_id')
        self.task_id = kwargs.pop('task_id')
        super(EditTaskForm, self).__init__(*args, **kwargs)

        if self.request == 'GET':
            weight = models.Task.objects.filter(assessment_id=self.assessment_id
                                                ).aggregate(Sum('weight'))['weight__sum']
            weight = float(weight or 0) - models.Task.objects.get(pk=self.task_id).weight
            self.fields['weight'] = forms.IntegerField(min_value=0.00, max_value=(100 - weight),
                                                       widget=forms.NumberInput(
                                                           attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Weight',
                                                               'step': 'any',
                                                           }
                                                       ))


class AddActivityForm(forms.ModelForm):
    class Meta:
        model = models.Activity
        exclude = ('task',)
        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Date'}),
            'duration': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Duration', 'step': 5}),
            'progress': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Progress'}),
            'comments': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Comments', 'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.task_id = kwargs.pop('task_id')
        super(AddActivityForm, self).__init__(*args, **kwargs)

        if self.request == 'GET':
            progress = models.Activity.objects.filter(task_id=self.task_id).aggregate(Sum('progress'))['progress__sum']
            progress = int(progress or 0)
            self.fields['progress'] = forms.IntegerField(min_value=0, max_value=100 - progress,
                                                         widget=forms.NumberInput(
                                                            attrs={
                                                               'class': 'form-control',
                                                               'placeholder': 'Progress',
                                                               'step': 5,
                                                            }
                                                         ))


class EditActivityForm(forms.ModelForm):
    class Meta:
        model = models.Activity
        exclude = ('task',)
        widgets = {
            'date': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Date'}),
            'duration': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Duration', 'step': 5}),
            'progress': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Progress', 'step': 5}),
            'comments': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Comments', 'rows': 5}),
        }
