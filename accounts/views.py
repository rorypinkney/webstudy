from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from splanner.forms import SignUpForm


def signup(request):
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			# authenticate function returns a user instance (it always succeeds in this case)
			user = authenticate(username=username, password=raw_password)
			# log the user in with login function and redirect them
			login(request, user)
			return redirect('home')
	else:
		form = SignUpForm()
	return render(request, 'registration/signup.html', {'form': form})






