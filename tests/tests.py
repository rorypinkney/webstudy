from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.contrib import auth
from splanner.models import *


class ModuleModelTest(TestCase):

    # Test that we can successfully create a module
    def create_module(self):
        user = User.objects.create(username="Dom_12", password="Pas_10")
        return Module.objects.create(name="Programming", user=user, weight=5)

    # test the name of an entry is a string
    def test_module_name(self):
        module = self.create_module()
        self.assertEqual(module.__str__(), module.name)


class AssesmentModelTest(TestCase):

    # Test that we can successfully create an assessment
    def create_assesment(self):
        user = User.objects.create(username="Dom_12", password="Pas_10")
        module = Module.objects.create(name="Dom", user=user)
        return Assessment.objects.create(name="AlbumDatabase", module=module, due_date='2012-12-12', weight=0)

    # test the name of an assessment is a string
    def test_assesment_name(self):
        user = User.objects.create(username="Dom_12", password="Pas_10")
        module = Module.objects.create(name="Dom", user=user, weight=20)
        assessment = Assessment.objects.create(name="AlbumDatabase", module=module, start_date='2019-05-15', due_date='2019-05-16', weight=5, period=1)
        self.assertEqual(assessment.__str__(), assessment.name)

class TaskModelTest(TestCase):

    # Test we can create a task
    def test_create_task(self):
        name = "Write the tests"
        user = User.objects.create(username="Dom_12", password="Pas_10")
        module = Module.objects.create(name="Dom", user=user, weight=20)
        assessment = Assessment.objects.create(name="AlbumDatabase", module=module, start_date='2019-05-15',
                                               due_date='2019-05-16', weight=0, period=1)
        part = 1
        type = 1
        weight = 1
        progress = 10
        return Task.objects.create(name=name, assessment=assessment, part=part, type=type, weight=weight, progress=progress)

    # test the name of an task is a string
    def test_assesment_name(self):
        name = "Write the tests"
        user = User.objects.create(username="Dom_12", password="Pas_10")
        module = Module.objects.create(name="Dom", user=user, weight=20)
        assessment = Assessment.objects.create(name="AlbumDatabase", module=module, start_date='2019-05-15',
                                               due_date='2019-05-16', weight=0, period=1)
        part = 1
        type = 1
        weight = 1
        progress = 10
        task = Task.objects.create(name=name, assessment=assessment, part=part, type=type, weight=weight,
                                   progress=progress)
        self.assertEqual(task.__str__(), task.name)


class ProjectTests(TestCase):

    # test that our homepage returns a successful http request
    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # create this user before every test so that every test can access it
    def setUp(self):
        self.user = get_user_model().objects.create_user(username = "Dom_12", password = "Pas_10")

    # test whether user login authenticates a user
    def test_login_user(self):
        self.client.login(username='Dom_12', password='Pas_10')
        user = auth.get_user(self.client)
        assert user.is_authenticated

    # test that the addModule option available on page after signing in
    def test_login_after_form(self):
        self.client.login(username='Dom_12', password='Pas_10')
        response = self.client.get('/')
        self.assertContains(response, "addModule")


    # test that the addModule option unavailable on page if not signed in
    def test_login_blocks_module(self):
        response = self.client.get('/')
        self.assertNotContains(response, "addModule")


class IntegrationTests(TestCase):

    # Testing that the user is logged and the module created is assigned to them
    def test_login_and_module(self):
        testuser = User.objects.create(username="Dom_12", password="Pas_10")
        if testuser.is_authenticated:
            module = Module.objects.create(name="Dom", user=testuser, weight = 20)
            self.assertEqual(module, Module.objects.get(user=User.objects.get(username="Dom_12")))


    # Testing that the user can create an account, login, create a module, add an assessment to the module
    # and that the assessment added to the module is equal to the assessment created
    def test_login_and_assessment(self):
        testuser = User.objects.create(username="Dom_12", password="Pas_10")
        if testuser.is_authenticated:
            module = Module.objects.create(name="Dom", user=testuser, weight=20)
            assessment = Assessment.objects.create(name="AlbumDatabase", module=module, start_date='2012-12-11',
                                                   due_date='2012-12-12', weight=0, period=1)
            self.assertEqual(assessment, Assessment.objects.get(module=module))


    # Testing creating a task and that it is assigned to the user
    def test_login_and_task(self):
        testuser = User.objects.create(username="Dom_12", password="Pas_10")
        if testuser.is_authenticated:
            module = Module.objects.create(name="Dom", user=testuser, weight=20)
            assessment = Assessment.objects.create(name="AlbumDatabase", module=module, start_date='2012-12-11',
                                                   due_date='2012-12-12', weight=0, period=1)
            name = "Write the tests"
            part = 1
            type = 1
            weight = 1
            progress = 10
            task = Task.objects.create(name=name, assessment=assessment, part=part, type=type, weight=weight,
                                       progress=progress)
            self.assertEqual(task, Task.objects.get(assessment=assessment))





