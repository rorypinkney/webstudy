**Software Engineering Coursework 2 - Web Based Study Planner**

Created using the Python Django framework with a sqlite database, this is a tool designed to assist users in planning their studies based on deadlines and their current progress.

Features:  
- Functioning signup with email confirmation  
- Simple Year hierarchy: Modules > Assessments > Tasks  
- Add Activities to tasks which track progress through  
- Progress propogates up the chain so you can see how far through your module you are  
- Display upcoming deadlines (2 weeks)  
- Simple AI to welcome and assist the user  
	
Group coursework for our second year 'Software Engineering' module. Our group received a mark of 80% for this work. 